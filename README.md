## UK Visa Requirement Check and Postcode Validation Project

This project is intended to automate Acceptance tests for the features mentioned below.


### Features Implemented:

* Confirm whether a visa is required to visit the UK
* Query a postcode and receive a 200 reponse

###Technologies used:

1. This project is developed using **Cucumber** and **Serenity** frameworks.
2. **Maven** is used to build the project.
3. **Serenity Rest** is used for API testing.
4. **Hamcrest** and **AssertJ** libraries have been used for Assertions.

### Notes:

* Project is configured to use *Chrome driver* for test execution.
* For feature **2**, i.e. API testing,  steps and page objects classes are not used/created to keep it simple.
