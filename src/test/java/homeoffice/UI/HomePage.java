package homeoffice.UI;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.gov.uk/check-uk-visa/y")
public class HomePage extends PageObject{

	@FindBy(id="response")
	public WebElementFacade selectNationality;
	
	@FindBy(css=".gem-c-button:nth-child(3)")
	public WebElementFacade nextStepButton;	

	@FindBy(xpath="//input[@name='response' and @value='study']", timeoutInSeconds="10")
	public WebElementFacade visitReasons;


	@FindBy(id="response-0")
	public WebElementFacade visitDuration;
	
	@FindBy(className="gem-c-heading", timeoutInSeconds="10")
	public WebElementFacade result;


	public void select_reason_for_UK_visit(String visitReason) {
		String reasonLocator = "//input[@name='response' and @value=" + visitReason + "]";
		findBy(reasonLocator).click();
		nextStepButton.waitUntilClickable();
		nextStepButton.click();
		
	}

	public void select_duration(String duration) {
	
		String durationLocator = "//input[@name='response' and @value=" + duration.replace(' ', '_') + "]";
		findBy(durationLocator).click();
		
	}

	public String getTheResult() {
		
		return findBy(".gem-c-heading   ").getText();
		
	}

	public void submitForm() {		
		if (nextStepButton.isPresent()){
			nextStepButton.waitUntilEnabled();
			nextStepButton.click();
		} 
	}

	public void selectNationality_From_Dropdown(String nationality) {
		
		selectNationality.selectByVisibleText(nationality);
		assertThat(selectNationality.getSelectedVisibleTextValue(), is(nationality));
//		selectNationality.sendKeys(Keys.TAB);
		nextStepButton.waitUntilClickable();
		nextStepButton.click();
		
	}
	
}
