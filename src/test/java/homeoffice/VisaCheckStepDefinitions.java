package homeoffice;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import homeoffice.steps.UKVisitor;
import net.thucydides.core.annotations.Steps;

public class VisaCheckStepDefinitions {
	
	@Steps
	UKVisitor user;
	
	@Given("^(?:.+) provides? a nationality of (.+)$")
	public void user_is_a_citizen_of_Japan(String nationality) {
		user.opensApplication();
		user.selects_the_nationality_as(nationality);
	}


	@Given("^user states he is (.+) with or visiting a partner or family$")
	public void user_states_he_is_not_travelling_with_or_visiting_a_partner_or_family(String isOrIsNotTravellingWith) {

		String yesOrNo;
		
		if(isOrIsNotTravellingWith.contains("not")) {
			yesOrNo = "no";
		}
		else {
		//  As the scenario where user selects "Yes" is not implemented user selection is defaulted to "No"		
			yesOrNo = "no";
		}
		user.statesHeIsOrIsNotTravellingWithPartnerOrFamily(yesOrNo);
		
	}


	@When("^(?:.+) selects? the reason (.+)$")
	public void user_selects_the_reason_for_visit_is(String visitReason) {
		user.selects_the_reason_for_visit_is(visitReason);
	}

	@When("^(?:.+) intends? to stay for (\"6 months or less\"|\"longer than 6 months\"|\"longer than six months\"|\"six months or less\")$")
	public void user_confirms_that_intends_to_stay_for(String duration){
		user.selects_the_duration_of_stay_as(duration.replace("6", "six"));
	}

	@When("^(?:.+) submits? the form$")
	public void user_submits_the_form() {
		user.submits_the_form();
		}

	@Then("^(?:.+) will be informed \"([^\"]*)\"$")
	public void user_will_be_informed(String expectedResult) {
		user.getTheResult(expectedResult);
	}



}
