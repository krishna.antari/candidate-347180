package homeoffice;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;


public class PostcodeCheckStepDefinitions {

	@Given("^I provide a postcode (.+)$")
	public void i_provide_a_postcode(String postcode) {

		Serenity.setSessionVariable("postcode").to(postcode);
	}


	@Then("^I get a (\\d+) reponse$")
	public void i_get_a_reponse(int responseCode) {
		String postcode = Serenity.sessionVariableCalled("postcode").toString();
		System.out.println("Postcode to be checked is :" + postcode);
		System.out.println("Expected response code is :" +responseCode);
		SerenityRest.when().get("http://api.postcodes.io/postcodes/{postcode}",postcode)
		.then().statusCode(responseCode);
	}
}
