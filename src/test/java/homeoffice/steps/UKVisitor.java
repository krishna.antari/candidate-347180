package homeoffice.steps;

import static org.assertj.core.api.Assertions.assertThat;
import homeoffice.UI.HomePage;
import net.thucydides.core.annotations.Step;

public class UKVisitor {

	HomePage visaCheckPage;
	
	@Step
	public void selects_the_nationality_as(String nationality) {
		
	    visaCheckPage.selectNationality_From_Dropdown(nationality);
		
	}

	@Step
	public void selects_the_reason_for_visit_is(String visitReason) {
	
		visaCheckPage.select_reason_for_UK_visit(visitReason.toLowerCase());
	}

	@Step
	public void selects_the_duration_of_stay_as(String duration) {
		visaCheckPage.select_duration(duration);
		
	}

	@Step
	public void submits_the_form() {
		
		visaCheckPage.submitForm();
		
	}

	@Step
	public void getTheResult(String expectedResult) {
		
		assertThat(visaCheckPage.getTheResult()).isEqualTo(expectedResult);
		
	}

	
	public void opensApplication() {

		visaCheckPage.open();
		if(visaCheckPage.findBy("//button[@type='submit' and contains(., 'Accept cookies')]").isCurrentlyVisible()) {
			visaCheckPage.findBy("//button[@type='submit' and contains(., 'Accept cookies')]").click();
		}		

		
	}

	public void statesHeIsOrIsNotTravellingWithPartnerOrFamily(String familyVisitOrTravel) {
		String reasonLocator = "//input[@name='response' and @value='" + familyVisitOrTravel + "']";
		visaCheckPage.findBy(reasonLocator).click();
		
		
	}

} 
