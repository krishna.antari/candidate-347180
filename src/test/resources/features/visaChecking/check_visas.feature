Feature: Confirm whether a visa is required to visit the UK

Scenario: Japanese citizen checking if visa is required to study in UK
Given user provides a nationality of Japan
When user selects the reason "Study"
And user intends to stay for "longer than six months"
And user submits the form
Then user will be informed "You’ll need a visa to study in the UK"

Scenario: Japanese citizen checking if visa is required to visit the UK as a tourist
Given I provide a nationality of Japan
And  I select the reason "Tourism"
When I submit the form
Then user will be informed "You won’t need a visa to come to the UK"

Scenario: Russian citizen checking if visa is required to visit UK as a tourist
Given Katarina provides a nationality of Russia
And user selects the reason "Tourism"
And user states he is travelling with or visiting a partner or family
When user submits the form
Then user will be informed "You’ll need a visa to come to the UK"

